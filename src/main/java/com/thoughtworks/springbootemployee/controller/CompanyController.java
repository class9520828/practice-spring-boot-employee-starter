package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.respository.CompanyRepository;
import com.thoughtworks.springbootemployee.respository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping()
    public Company createCompany (@RequestBody Company company) {
        companyRepository.createCompany(company);
        return company;
    }

    @GetMapping()
    public List<Company> getCompaniesList() {
        return companyRepository.getCompaniesList();
    }

    @GetMapping("/{companyId}")
    public Company getCompanyById(@PathVariable long companyId) {
        return companyRepository.getCompanyById(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable long companyId) {
        return employeeRepository.findByCompanyId(companyId);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable long companyId) {
        companyRepository.deleteCompany(companyId);
        employeeRepository.fireEmployees(companyId);
    }
    @GetMapping(value = "/{companyId}", params = {"name"})
    public Company updateCompany(@PathVariable long companyId, @RequestParam String name) {
        return this.companyRepository.updateCompany(companyId, name);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> showCompanyPage(@RequestParam int page, @RequestParam int size) {
        return companyRepository.showCompanyPage(page, size);

    }



}
