package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.respository.CompanyRepository;
import com.thoughtworks.springbootemployee.respository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        employeeRepository.createEmployee(employee);
        return employee;

    }

    @GetMapping()
    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeesList();
    }

    @GetMapping("/{employeeId}")
    public Employee getEmployeeById(@PathVariable long employeeId) {
        return employeeRepository.getEmployeeById(employeeId);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeeByGender(String gender) {
        return employeeRepository.getEmployeeByGender(gender);
    }

    @PutMapping("/{employeeId}")
    public Employee updateEmployee (@PathVariable long employeeId, @RequestParam int age, @RequestParam int salary) {
        return employeeRepository.updateEmployee(employeeId, age, salary);

    }

    @PutMapping(value = "/{employeeId}", params = {"companyId"})
    public Employee hireEmployee (@PathVariable long employeeId, @RequestParam long companyId) {
        return employeeRepository.hireEmployee(employeeId, companyId);

    }

    @DeleteMapping("/{employeeId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee (@PathVariable Long employeeId) {
        employeeRepository.deleteEmployee(employeeId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> showEmployeePge(@RequestParam int page, @RequestParam int size) {
        return employeeRepository.showEmployeePage(page, size);
    }


}
