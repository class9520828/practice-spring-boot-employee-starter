package com.thoughtworks.springbootemployee.controller;

public class Employee {
    public static final int NO_COMPANY = 0;
    private long id;
    private String name;
    private int age;
    private String gender;
    private int salary;
    private long companyId = NO_COMPANY;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public int getSalary() {
        return salary;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public long getCompanyId() {
        return companyId;
    }
}
