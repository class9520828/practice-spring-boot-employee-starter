package com.thoughtworks.springbootemployee.respository;

import com.thoughtworks.springbootemployee.controller.Company;
import com.thoughtworks.springbootemployee.controller.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private List<Company> companies = new ArrayList<>();

    public void createCompany(Company company) {
        company.setId(generateId());
        this.companies.add(company);

    }

    public long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(company -> company.getId() + 1)
                .orElse(1L);
    }


    public List<Company> getCompaniesList() {
        return this.companies;
    }

    public Company getCompanyById(long companyId) {
        return this.companies.stream()
                .filter(company -> Objects.equals(company.getId(), companyId))
                .findFirst()
                .orElse(null);
    }

    public Company updateCompany(long companyId, String name) {
        for (Company company : this.companies) {
            if (Objects.equals(company.getId(), companyId)) {
                company.setName(name);
                return company;
            }
        }
        return null;
    }

    public void deleteCompany(long companyId) {
        this.companies = companies.stream()
                .filter(company -> !Objects.equals(companyId, company.getId()))
                .collect(Collectors.toList());

    }

    public List<Company> showCompanyPage(int page, int size) {
        return this.companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
