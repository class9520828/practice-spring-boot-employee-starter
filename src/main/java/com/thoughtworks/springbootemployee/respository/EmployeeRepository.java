package com.thoughtworks.springbootemployee.respository;

import com.thoughtworks.springbootemployee.controller.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    public void createEmployee(Employee employee) {
        employee.setId(generateId());
        this.employees.add(employee);

    }

    public long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> getEmployeesList() {
        return this.employees;
    }

    public Employee getEmployeeById(long employeeId) {
        return this.employees.stream().filter(employee -> Objects.equals(employee.getId(), employeeId)).findFirst().orElse(null);
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return this.employees.stream().filter(employee -> Objects.equals(employee.getGender(), gender)).collect(Collectors.toList());
    }

    public Employee updateEmployee(long employeeId, int age, int salary) {
        for (Employee employee : this.employees) {
            if (Objects.equals(employee.getId(), employeeId)) {
                employee.setAge(age);
                employee.setSalary(salary);
                return employee;
            }
        }
        return null;
    }

    public void deleteEmployee(long employeeId) {
        this.employees = employees.stream()
                .filter(employee -> !Objects.equals(employeeId, employee.getId()))
                .collect(Collectors.toList());
    }

    public List<Employee> showEmployeePage(int page, int size) {
        return this.employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee hireEmployee(long employeeId, long companyId) {
        for (Employee employee : this.employees) {
            if (Objects.equals(employee.getId(), employeeId)) {
                employee.setCompanyId(companyId);
                return employee;
            }
        }
        return null;
    }

    public List<Employee> findByCompanyId(long companyId) {
        return this.employees.stream().filter(employee -> Objects.equals(employee.getCompanyId(), companyId)).collect(Collectors.toList());
    }

    public void fireEmployees(long companyId) {
        employees.stream()
                .filter(employee -> Objects.equals(companyId, employee.getCompanyId()))
                .forEach(employee -> employee.setCompanyId(Employee.NO_COMPANY));
    }

}
