O(Objective):
+ In the morning, our group conducted a Code Review, which helped us become familiar with the refactoring specifications and processes once again.
+ Through the group presentation of Concept Map activities, I became more familiar with the concepts of TDD, OOP, and Refactor.
+ In the afternoon, I studied the Spring Boot framework and learned how to create a basic Speing Boot project, gaining a basic understanding of the Spring framework.
+ I have learned the basic concepts of HTTP and understood the basic interpretation methods.

R(Reflective):  
&emsp;&emsp;Fruitful

I(Interpretive):  
&emsp;&emsp;I am not very proficient in some usage and properties of the Spring Boot framework, and I cannot keep up with the teacher's pace well during classroom practice. &emsp;I need to continue practicing in the future. &emsp;Today's review of knowledge related to TDD, OOP, Refactor, and others has led me to discover loopholes in my understanding of these knowledge. &emsp;I will continue to learn and master them proficiently in the future.

D(Decision):
&emsp;&emsp;Continue to master the previously learned knowledge through practice.
